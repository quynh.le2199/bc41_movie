import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieServ } from '../../service/movieService';
import { Progress, Space } from 'antd';

export default function DetailPage() {
  let params = useParams();
  const [movie, setMovie] = useState({})
  useEffect(() => {
    // movieServ.getDetailMovie(params.id)
    //          .then(res => { //đợi khi nào có data thì log ra nên khi chuyển sang cách dưới mình sẽ dùng await
    //           console.log(res);
    //          })
    //          .catch(err => {
    //           console.log(err);
    //          });

    let fetchDetail = async() => { // muốn dùng đc await thì phải có async()
        try { //giống như then ở cách trên
            let res = await movieServ.getDetailMovie(params.id);
            //console.log("🚀 ~ file: DetailPage.js:21 ~ fetchDetail ~ res:", res) //nhờ có await nên console.log chạy sau await
            setMovie(res.data.content)
        }
        catch (error) {}
    }
    fetchDetail();
  }, [])
  return (
    <div className='container flex'>
        <img className='w-1/4' src={movie.hinhAnh} alt="" />
        <div className="p-5 space-y-10">
            <h2 className='text-xl font-medium'>{movie.tenPhim}</h2>
            <p className='text-xs text-gray-600'>{movie.moTa}</p>
            <Progress strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }} type="circle" percent={movie.danhGia * 10} format={(percent) => `${percent / 10} Điểm`} />
            <NavLink to={`/booking/${movie.maPhim}`} className="px-5 py-2 rounded bg-red-500 text-white">Mua vé</NavLink>
        </div>
    </div>
  )
}
