import React from 'react'

export default function NotFoundPage() {
  return (
    <div className="flex justify-center items-center text-center">
        <h2 className='text-5xl font-black text-red-500 animate-bounce transform translate-y-50'>404 <br /> Not Found Page</h2>
    </div>

  )
}
