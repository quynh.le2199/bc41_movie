import moment from 'moment/moment'
import React from 'react'

export default function ItemTabsMovie({phim}) {
  // console.log("🚀 ~ file: ItemTabsMovie.js:4 ~ ItemTabsMovie ~ phim:", phim)
  return (
    <div className='flex items-center space-x-5 border-gray-200 border-b pb-3'>
        <img src={phim.hinhAnh} className='w-28 h-48 object-cover object-top rounded' alt="" />
        <div>
            <h5 className='font-medium text-xl mb-5'>{phim.tenPhim}</h5>
            <div className='grid grid-cols-3 gap-4'>{phim.lstLichChieuTheoPhim.slice(0, 6).map((item) => {
                return <span className='rounded p-3 text-white bg-gray-300 font-medium' key={item.maLichChieu}>{moment(item.ngayChieuGioChieu).format("DD-mm-yyyy ~ hh:mm")}</span>
            })}</div>
        </div>
    </div>
  )
}
